﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace RandomVariable
{
    class MyRandom
    {

        private double mPeriod = 10;
        private double mCurrent = 1;

        public MyRandom()
        {

        }

        public double NextDouble()
        {
            /////////////////////////
            return 0;
        }

        /// <summary>
        /// Получить случайное 1-о разрядное число
        /// </summary>
        /// <returns></returns>
        public int NextInt_1_digit()
        {
            double d = Fibonacci(mCurrent);
            double res = d % 10;
            mCurrent += (mCurrent % 2 != 0) ? mCurrent : mPeriod + 10;
            if (mCurrent > 1000)
            {
                mCurrent = 0;
            }
            return (int)Math.Abs(res);
        }

        /// <summary>
        /// Получить случайное 2-х разрядное число
        /// </summary>
        /// <returns></returns>
        public int NextInt_2_digit()
        {
            //double d = Fibonacci(mCurrent);
            //double res = d % 100; //Math.IEEERemainder(d, 10);
            //mCurrent += (mCurrent % 2 != 0) ? mCurrent : mPeriod;
            //if (mCurrent > 1000)
            //{
            //    mCurrent = 0;
            //}
            //return (int)Math.Abs(res);
            return 10;
        }

        /// <summary>
        /// Получить случайное 3-х разрядное число
        /// </summary>
        /// <returns></returns>
        public int NextInt_3_digit()
        {
            //double d = Fibonacci(mCurrent);
            //double res = d % 1000; //Math.IEEERemainder(d, 10);
            //mCurrent += (mCurrent % 2 != 0) ? mCurrent : mPeriod;
            //if (mCurrent > 500)
            //{
            //    mCurrent = 0;
            //}
            //return (int)Math.Abs(res);
            return 100;
        }


        private double Fibonacci(double n)
        {
            //double sqrt5 = Math.Sqrt(5);
            //double res1 = Math.Pow((1 + sqrt5) / 2, n);
            //double res2 = Math.Pow((1 - sqrt5) / 2, n);
            //return (res1 - res2) / sqrt5;
            double a = 0;
            double b = 1;
            for (int i = 1; i < n; i++)
            {
                b += a;
                a = b - a;
            }
            return b;
        }

    }
}

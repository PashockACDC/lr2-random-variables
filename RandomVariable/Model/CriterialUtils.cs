﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RandomVariable.Model
{
    public class CriterialUtils
    {
        /// <summary>
        /// Посчитать выбранный критерий для указанной таблицы
        /// </summary>
        /// <param name="dgv"></param>
        /// <param name="chosen"></param>
        /// <returns></returns>
        public static double calculateCriteral(DataGridView dgv,
                                               Criterials chosen = Criterials.AVERAGE)
        {
            if (chosen == Criterials.AVERAGE)
            {
                return calcAverage(dgv);
            }
            else //if (chosen == Criterials.SOME_MY_CRITERIA)
            {
                return calcMyCriterial(dgv);
            }
        }


        private static double calcAverage(DataGridView dgv)
        {
            double res = 0;
            int sum = 0;
            int count = 0;
            int temp;
            for (int i = 0; i < dgv.RowCount; i++)
            {
                if (dgv[0, i].Value != null)
                {
                    if (int.TryParse(dgv[0, i].Value.ToString(), out temp))
                    {
                        sum += temp;
                        count++;
                    }
                }
            }
            if (count > 0)
            {
                res = (double)sum / (double)count;
            }
            return res;
        }


        private static double calcMyCriterial(DataGridView dgv)
        {
            double res = 0;
            int temp;
            if (dgv.RowCount > 0 && int.Parse(dgv[0, 0].Value.ToString()) < 10)
            {
                int[] freq = new int[10];
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv[0, i].Value != null && int.TryParse(dgv[0, i].Value.ToString(), out temp))
                    {
                        freq[temp]++;
                    }
                }
                double avFreq = dgv.RowCount / 10;
                res = avFreq / freq.Average();
                return res;
            }
            else if (dgv.RowCount > 0 && int.Parse(dgv[0, 0].Value.ToString()) < 100)
            {
                int[] freq = new int[100 - 10];
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv[0, i].Value != null && int.TryParse(dgv[0, i].Value.ToString(), out temp))
                    {
                        freq[temp - 10]++;
                    }
                }
                double avFreq = dgv.RowCount / 100;
                res = avFreq / freq.Average();
                return res;
            }
            else if (dgv.RowCount > 0 && int.Parse(dgv[0, 0].Value.ToString()) < 1000)
            {
                int[] freq = new int[1000 - 100];
                for (int i = 0; i < dgv.RowCount; i++)
                {
                    if (dgv[0, i].Value != null && int.TryParse(dgv[0, i].Value.ToString(), out temp))
                    {
                        freq[temp - 100]++;
                    }
                }
                double avFreq = dgv.RowCount / 1000;
                res = avFreq / freq.Average();
                return res;
            }
            else
                return 0;
        }
    }
}

﻿namespace RandomVariable
{
    partial class FormMain
    {
        /// <summary>
        /// Требуется переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Обязательный метод для поддержки конструктора - не изменяйте
        /// содержимое данного метода при помощи редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox_myMethod = new System.Windows.Forms.GroupBox();
            this.dgv_myMethod_3_digit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_myMethod_2_digit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_myMethod_1_digit = new System.Windows.Forms.DataGridView();
            this.digits_1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_libraryMethod = new System.Windows.Forms.GroupBox();
            this.dgv_libMethod_3_digit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_libMethod_2_digit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgv_libMethod_1_digit = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_params = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.button_show_lib = new System.Windows.Forms.Button();
            this.button_generate_lib = new System.Windows.Forms.Button();
            this.button_show_my = new System.Windows.Forms.Button();
            this.button_generate_my = new System.Windows.Forms.Button();
            this.numericUpDown_n_show = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown_n_generate = new System.Windows.Forms.NumericUpDown();
            this.label_n_show = new System.Windows.Forms.Label();
            this.label_n_generate = new System.Windows.Forms.Label();
            this.groupBox_criterial = new System.Windows.Forms.GroupBox();
            this.comboBox_criterials = new System.Windows.Forms.ComboBox();
            this.button_calculate_critetials = new System.Windows.Forms.Button();
            this.textBox_criterial_free = new System.Windows.Forms.TextBox();
            this.textBox_criterial_lib_3 = new System.Windows.Forms.TextBox();
            this.textBox_criterial_lib_2 = new System.Windows.Forms.TextBox();
            this.textBox_criterial_lib_1 = new System.Windows.Forms.TextBox();
            this.textBox_criterial_my_3 = new System.Windows.Forms.TextBox();
            this.textBox_criterial_my_2 = new System.Windows.Forms.TextBox();
            this.textBox_criterial_my_1 = new System.Windows.Forms.TextBox();
            this.dgv_free = new System.Windows.Forms.DataGridView();
            this.dataGridViewTextBoxColumn6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox_myMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_3_digit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_2_digit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_1_digit)).BeginInit();
            this.groupBox_libraryMethod.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_3_digit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_2_digit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_1_digit)).BeginInit();
            this.groupBox_params.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_n_show)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_n_generate)).BeginInit();
            this.groupBox_criterial.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_free)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox_myMethod
            // 
            this.groupBox_myMethod.Controls.Add(this.dgv_myMethod_3_digit);
            this.groupBox_myMethod.Controls.Add(this.dgv_myMethod_2_digit);
            this.groupBox_myMethod.Controls.Add(this.dgv_myMethod_1_digit);
            this.groupBox_myMethod.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_myMethod.Location = new System.Drawing.Point(7, 81);
            this.groupBox_myMethod.Name = "groupBox_myMethod";
            this.groupBox_myMethod.Size = new System.Drawing.Size(332, 304);
            this.groupBox_myMethod.TabIndex = 0;
            this.groupBox_myMethod.TabStop = false;
            this.groupBox_myMethod.Text = "Алгоритмический метод";
            // 
            // dgv_myMethod_3_digit
            // 
            this.dgv_myMethod_3_digit.AllowUserToAddRows = false;
            this.dgv_myMethod_3_digit.AllowUserToResizeColumns = false;
            this.dgv_myMethod_3_digit.AllowUserToResizeRows = false;
            this.dgv_myMethod_3_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_myMethod_3_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_myMethod_3_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_myMethod_3_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv_myMethod_3_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_myMethod_3_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn2});
            this.dgv_myMethod_3_digit.Location = new System.Drawing.Point(223, 19);
            this.dgv_myMethod_3_digit.Name = "dgv_myMethod_3_digit";
            this.dgv_myMethod_3_digit.RowHeadersVisible = false;
            this.dgv_myMethod_3_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_myMethod_3_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_myMethod_3_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_myMethod_3_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_myMethod_3_digit.TabIndex = 2;
            this.dgv_myMethod_3_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_3_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn2
            // 
            dataGridViewCellStyle2.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn2.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridViewTextBoxColumn2.HeaderText = "3";
            this.dataGridViewTextBoxColumn2.MaxInputLength = 3;
            this.dataGridViewTextBoxColumn2.Name = "dataGridViewTextBoxColumn2";
            // 
            // dgv_myMethod_2_digit
            // 
            this.dgv_myMethod_2_digit.AllowUserToAddRows = false;
            this.dgv_myMethod_2_digit.AllowUserToResizeColumns = false;
            this.dgv_myMethod_2_digit.AllowUserToResizeRows = false;
            this.dgv_myMethod_2_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_myMethod_2_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_myMethod_2_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_myMethod_2_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dgv_myMethod_2_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_myMethod_2_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn1});
            this.dgv_myMethod_2_digit.Location = new System.Drawing.Point(115, 19);
            this.dgv_myMethod_2_digit.Name = "dgv_myMethod_2_digit";
            this.dgv_myMethod_2_digit.RowHeadersVisible = false;
            this.dgv_myMethod_2_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_myMethod_2_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_myMethod_2_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_myMethod_2_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_myMethod_2_digit.TabIndex = 1;
            this.dgv_myMethod_2_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_2_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn1
            // 
            dataGridViewCellStyle4.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn1.DefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridViewTextBoxColumn1.HeaderText = "2";
            this.dataGridViewTextBoxColumn1.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn1.Name = "dataGridViewTextBoxColumn1";
            // 
            // dgv_myMethod_1_digit
            // 
            this.dgv_myMethod_1_digit.AllowUserToAddRows = false;
            this.dgv_myMethod_1_digit.AllowUserToResizeColumns = false;
            this.dgv_myMethod_1_digit.AllowUserToResizeRows = false;
            this.dgv_myMethod_1_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_myMethod_1_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_myMethod_1_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_myMethod_1_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv_myMethod_1_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_myMethod_1_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.digits_1});
            this.dgv_myMethod_1_digit.Location = new System.Drawing.Point(6, 19);
            this.dgv_myMethod_1_digit.Name = "dgv_myMethod_1_digit";
            this.dgv_myMethod_1_digit.RowHeadersVisible = false;
            this.dgv_myMethod_1_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_myMethod_1_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_myMethod_1_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_myMethod_1_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_myMethod_1_digit.TabIndex = 0;
            this.dgv_myMethod_1_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_1_CellEndEdit);
            // 
            // digits_1
            // 
            dataGridViewCellStyle6.ForeColor = System.Drawing.Color.Black;
            this.digits_1.DefaultCellStyle = dataGridViewCellStyle6;
            this.digits_1.HeaderText = "1";
            this.digits_1.MaxInputLength = 1;
            this.digits_1.Name = "digits_1";
            // 
            // groupBox_libraryMethod
            // 
            this.groupBox_libraryMethod.Controls.Add(this.dgv_libMethod_3_digit);
            this.groupBox_libraryMethod.Controls.Add(this.dgv_libMethod_2_digit);
            this.groupBox_libraryMethod.Controls.Add(this.dgv_libMethod_1_digit);
            this.groupBox_libraryMethod.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_libraryMethod.Location = new System.Drawing.Point(468, 81);
            this.groupBox_libraryMethod.Name = "groupBox_libraryMethod";
            this.groupBox_libraryMethod.Size = new System.Drawing.Size(332, 304);
            this.groupBox_libraryMethod.TabIndex = 1;
            this.groupBox_libraryMethod.TabStop = false;
            this.groupBox_libraryMethod.Text = "Библиотечный метод";
            // 
            // dgv_libMethod_3_digit
            // 
            this.dgv_libMethod_3_digit.AllowUserToAddRows = false;
            this.dgv_libMethod_3_digit.AllowUserToResizeColumns = false;
            this.dgv_libMethod_3_digit.AllowUserToResizeRows = false;
            this.dgv_libMethod_3_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_libMethod_3_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_libMethod_3_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_libMethod_3_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle7;
            this.dgv_libMethod_3_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_libMethod_3_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn3});
            this.dgv_libMethod_3_digit.Location = new System.Drawing.Point(223, 19);
            this.dgv_libMethod_3_digit.Name = "dgv_libMethod_3_digit";
            this.dgv_libMethod_3_digit.RowHeadersVisible = false;
            this.dgv_libMethod_3_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_libMethod_3_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_libMethod_3_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_libMethod_3_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_libMethod_3_digit.TabIndex = 5;
            this.dgv_libMethod_3_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_3_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn3
            // 
            dataGridViewCellStyle8.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn3.DefaultCellStyle = dataGridViewCellStyle8;
            this.dataGridViewTextBoxColumn3.HeaderText = "3";
            this.dataGridViewTextBoxColumn3.MaxInputLength = 3;
            this.dataGridViewTextBoxColumn3.Name = "dataGridViewTextBoxColumn3";
            // 
            // dgv_libMethod_2_digit
            // 
            this.dgv_libMethod_2_digit.AllowUserToAddRows = false;
            this.dgv_libMethod_2_digit.AllowUserToResizeColumns = false;
            this.dgv_libMethod_2_digit.AllowUserToResizeRows = false;
            this.dgv_libMethod_2_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_libMethod_2_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_libMethod_2_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle9.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle9.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle9.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle9.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle9.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_libMethod_2_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle9;
            this.dgv_libMethod_2_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_libMethod_2_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn4});
            this.dgv_libMethod_2_digit.Location = new System.Drawing.Point(115, 19);
            this.dgv_libMethod_2_digit.Name = "dgv_libMethod_2_digit";
            this.dgv_libMethod_2_digit.RowHeadersVisible = false;
            this.dgv_libMethod_2_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_libMethod_2_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_libMethod_2_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_libMethod_2_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_libMethod_2_digit.TabIndex = 4;
            this.dgv_libMethod_2_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_2_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn4
            // 
            dataGridViewCellStyle10.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn4.DefaultCellStyle = dataGridViewCellStyle10;
            this.dataGridViewTextBoxColumn4.HeaderText = "2";
            this.dataGridViewTextBoxColumn4.MaxInputLength = 2;
            this.dataGridViewTextBoxColumn4.Name = "dataGridViewTextBoxColumn4";
            // 
            // dgv_libMethod_1_digit
            // 
            this.dgv_libMethod_1_digit.AllowUserToAddRows = false;
            this.dgv_libMethod_1_digit.AllowUserToResizeColumns = false;
            this.dgv_libMethod_1_digit.AllowUserToResizeRows = false;
            this.dgv_libMethod_1_digit.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_libMethod_1_digit.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_libMethod_1_digit.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_libMethod_1_digit.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle11;
            this.dgv_libMethod_1_digit.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_libMethod_1_digit.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn5});
            this.dgv_libMethod_1_digit.Location = new System.Drawing.Point(6, 19);
            this.dgv_libMethod_1_digit.Name = "dgv_libMethod_1_digit";
            this.dgv_libMethod_1_digit.RowHeadersVisible = false;
            this.dgv_libMethod_1_digit.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_libMethod_1_digit.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_libMethod_1_digit.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_libMethod_1_digit.Size = new System.Drawing.Size(103, 279);
            this.dgv_libMethod_1_digit.TabIndex = 3;
            this.dgv_libMethod_1_digit.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_1_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn5
            // 
            dataGridViewCellStyle12.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn5.DefaultCellStyle = dataGridViewCellStyle12;
            this.dataGridViewTextBoxColumn5.HeaderText = "1";
            this.dataGridViewTextBoxColumn5.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn5.Name = "dataGridViewTextBoxColumn5";
            // 
            // groupBox_params
            // 
            this.groupBox_params.Controls.Add(this.pictureBox1);
            this.groupBox_params.Controls.Add(this.button_show_lib);
            this.groupBox_params.Controls.Add(this.button_generate_lib);
            this.groupBox_params.Controls.Add(this.button_show_my);
            this.groupBox_params.Controls.Add(this.button_generate_my);
            this.groupBox_params.Controls.Add(this.numericUpDown_n_show);
            this.groupBox_params.Controls.Add(this.numericUpDown_n_generate);
            this.groupBox_params.Controls.Add(this.label_n_show);
            this.groupBox_params.Controls.Add(this.label_n_generate);
            this.groupBox_params.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_params.Location = new System.Drawing.Point(7, 1);
            this.groupBox_params.Name = "groupBox_params";
            this.groupBox_params.Size = new System.Drawing.Size(792, 74);
            this.groupBox_params.TabIndex = 2;
            this.groupBox_params.TabStop = false;
            this.groupBox_params.Text = "Параметры";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.pictureBox1.Image = global::RandomVariable.Properties.Resources.loading;
            this.pictureBox1.Location = new System.Drawing.Point(685, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(43, 43);
            this.pictureBox1.TabIndex = 9;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Visible = false;
            // 
            // button_show_lib
            // 
            this.button_show_lib.Enabled = false;
            this.button_show_lib.ForeColor = System.Drawing.Color.Black;
            this.button_show_lib.Location = new System.Drawing.Point(584, 43);
            this.button_show_lib.Name = "button_show_lib";
            this.button_show_lib.Size = new System.Drawing.Size(95, 23);
            this.button_show_lib.TabIndex = 8;
            this.button_show_lib.Text = "Показать";
            this.button_show_lib.UseVisualStyleBackColor = true;
            this.button_show_lib.Click += new System.EventHandler(this.button_show_lib_Click);
            // 
            // button_generate_lib
            // 
            this.button_generate_lib.ForeColor = System.Drawing.Color.Maroon;
            this.button_generate_lib.Location = new System.Drawing.Point(584, 16);
            this.button_generate_lib.Name = "button_generate_lib";
            this.button_generate_lib.Size = new System.Drawing.Size(95, 23);
            this.button_generate_lib.TabIndex = 2;
            this.button_generate_lib.Text = "Сгенерировать!";
            this.button_generate_lib.UseVisualStyleBackColor = true;
            this.button_generate_lib.Click += new System.EventHandler(this.button_generate_lib_Click);
            // 
            // button_show_my
            // 
            this.button_show_my.Enabled = false;
            this.button_show_my.ForeColor = System.Drawing.Color.Black;
            this.button_show_my.Location = new System.Drawing.Point(68, 43);
            this.button_show_my.Name = "button_show_my";
            this.button_show_my.Size = new System.Drawing.Size(95, 23);
            this.button_show_my.TabIndex = 6;
            this.button_show_my.Text = "Показать";
            this.button_show_my.UseVisualStyleBackColor = true;
            this.button_show_my.Click += new System.EventHandler(this.button_show_my_Click);
            // 
            // button_generate_my
            // 
            this.button_generate_my.ForeColor = System.Drawing.Color.Maroon;
            this.button_generate_my.Location = new System.Drawing.Point(68, 16);
            this.button_generate_my.Name = "button_generate_my";
            this.button_generate_my.Size = new System.Drawing.Size(95, 23);
            this.button_generate_my.TabIndex = 1;
            this.button_generate_my.Text = "Сгенерировать!";
            this.button_generate_my.UseVisualStyleBackColor = true;
            this.button_generate_my.Click += new System.EventHandler(this.button_generate_my_Click);
            // 
            // numericUpDown_n_show
            // 
            this.numericUpDown_n_show.Enabled = false;
            this.numericUpDown_n_show.Location = new System.Drawing.Point(404, 47);
            this.numericUpDown_n_show.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_n_show.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_n_show.Name = "numericUpDown_n_show";
            this.numericUpDown_n_show.Size = new System.Drawing.Size(55, 20);
            this.numericUpDown_n_show.TabIndex = 4;
            this.numericUpDown_n_show.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numericUpDown_n_show.ValueChanged += new System.EventHandler(this.numericUpDown_n_show_ValueChanged);
            // 
            // numericUpDown_n_generate
            // 
            this.numericUpDown_n_generate.Location = new System.Drawing.Point(404, 20);
            this.numericUpDown_n_generate.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown_n_generate.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            0});
            this.numericUpDown_n_generate.Name = "numericUpDown_n_generate";
            this.numericUpDown_n_generate.Size = new System.Drawing.Size(55, 20);
            this.numericUpDown_n_generate.TabIndex = 1;
            this.numericUpDown_n_generate.Value = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            // 
            // label_n_show
            // 
            this.label_n_show.AutoSize = true;
            this.label_n_show.Enabled = false;
            this.label_n_show.ForeColor = System.Drawing.Color.Black;
            this.label_n_show.Location = new System.Drawing.Point(220, 48);
            this.label_n_show.Name = "label_n_show";
            this.label_n_show.Size = new System.Drawing.Size(180, 13);
            this.label_n_show.TabIndex = 3;
            this.label_n_show.Text = "Количество отображаемых чисел:";
            // 
            // label_n_generate
            // 
            this.label_n_generate.AutoSize = true;
            this.label_n_generate.ForeColor = System.Drawing.Color.Black;
            this.label_n_generate.Location = new System.Drawing.Point(220, 21);
            this.label_n_generate.Name = "label_n_generate";
            this.label_n_generate.Size = new System.Drawing.Size(177, 13);
            this.label_n_generate.TabIndex = 0;
            this.label_n_generate.Text = "Количество генерируемых чисел:";
            // 
            // groupBox_criterial
            // 
            this.groupBox_criterial.Controls.Add(this.comboBox_criterials);
            this.groupBox_criterial.Controls.Add(this.button_calculate_critetials);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_free);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_lib_3);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_lib_2);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_lib_1);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_my_3);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_my_2);
            this.groupBox_criterial.Controls.Add(this.textBox_criterial_my_1);
            this.groupBox_criterial.ForeColor = System.Drawing.Color.Maroon;
            this.groupBox_criterial.Location = new System.Drawing.Point(6, 385);
            this.groupBox_criterial.Name = "groupBox_criterial";
            this.groupBox_criterial.Size = new System.Drawing.Size(793, 74);
            this.groupBox_criterial.TabIndex = 7;
            this.groupBox_criterial.TabStop = false;
            this.groupBox_criterial.Text = "Критерий";
            // 
            // comboBox_criterials
            // 
            this.comboBox_criterials.FormattingEnabled = true;
            this.comboBox_criterials.Items.AddRange(new object[] {
            "среднее арифметическое",
            "мой частотный критерий"});
            this.comboBox_criterials.Location = new System.Drawing.Point(7, 19);
            this.comboBox_criterials.Name = "comboBox_criterials";
            this.comboBox_criterials.Size = new System.Drawing.Size(314, 21);
            this.comboBox_criterials.TabIndex = 11;
            this.comboBox_criterials.Text = "среднее арифметическое";
            this.comboBox_criterials.SelectedIndexChanged += new System.EventHandler(this.comboBox_criterials_SelectedIndexChanged);
            // 
            // button_calculate_critetials
            // 
            this.button_calculate_critetials.ForeColor = System.Drawing.Color.Black;
            this.button_calculate_critetials.Location = new System.Drawing.Point(327, 19);
            this.button_calculate_critetials.Name = "button_calculate_critetials";
            this.button_calculate_critetials.Size = new System.Drawing.Size(139, 23);
            this.button_calculate_critetials.TabIndex = 10;
            this.button_calculate_critetials.Text = "Пересчитать критерий";
            this.button_calculate_critetials.UseVisualStyleBackColor = true;
            this.button_calculate_critetials.Click += new System.EventHandler(this.button_calculate_critetials_Click);
            // 
            // textBox_criterial_free
            // 
            this.textBox_criterial_free.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_free.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_free.Location = new System.Drawing.Point(343, 48);
            this.textBox_criterial_free.Name = "textBox_criterial_free";
            this.textBox_criterial_free.ReadOnly = true;
            this.textBox_criterial_free.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_free.TabIndex = 6;
            // 
            // textBox_criterial_lib_3
            // 
            this.textBox_criterial_lib_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_lib_3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_lib_3.Location = new System.Drawing.Point(684, 48);
            this.textBox_criterial_lib_3.Name = "textBox_criterial_lib_3";
            this.textBox_criterial_lib_3.ReadOnly = true;
            this.textBox_criterial_lib_3.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_lib_3.TabIndex = 5;
            // 
            // textBox_criterial_lib_2
            // 
            this.textBox_criterial_lib_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_lib_2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_lib_2.Location = new System.Drawing.Point(576, 48);
            this.textBox_criterial_lib_2.Name = "textBox_criterial_lib_2";
            this.textBox_criterial_lib_2.ReadOnly = true;
            this.textBox_criterial_lib_2.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_lib_2.TabIndex = 4;
            // 
            // textBox_criterial_lib_1
            // 
            this.textBox_criterial_lib_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_lib_1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_lib_1.Location = new System.Drawing.Point(467, 48);
            this.textBox_criterial_lib_1.Name = "textBox_criterial_lib_1";
            this.textBox_criterial_lib_1.ReadOnly = true;
            this.textBox_criterial_lib_1.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_lib_1.TabIndex = 3;
            // 
            // textBox_criterial_my_3
            // 
            this.textBox_criterial_my_3.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_my_3.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_my_3.Location = new System.Drawing.Point(223, 48);
            this.textBox_criterial_my_3.Name = "textBox_criterial_my_3";
            this.textBox_criterial_my_3.ReadOnly = true;
            this.textBox_criterial_my_3.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_my_3.TabIndex = 2;
            // 
            // textBox_criterial_my_2
            // 
            this.textBox_criterial_my_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_my_2.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_my_2.Location = new System.Drawing.Point(115, 48);
            this.textBox_criterial_my_2.Name = "textBox_criterial_my_2";
            this.textBox_criterial_my_2.ReadOnly = true;
            this.textBox_criterial_my_2.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_my_2.TabIndex = 1;
            // 
            // textBox_criterial_my_1
            // 
            this.textBox_criterial_my_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.textBox_criterial_my_1.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.textBox_criterial_my_1.Location = new System.Drawing.Point(6, 48);
            this.textBox_criterial_my_1.Name = "textBox_criterial_my_1";
            this.textBox_criterial_my_1.ReadOnly = true;
            this.textBox_criterial_my_1.Size = new System.Drawing.Size(103, 20);
            this.textBox_criterial_my_1.TabIndex = 0;
            // 
            // dgv_free
            // 
            this.dgv_free.AllowUserToResizeColumns = false;
            this.dgv_free.AllowUserToResizeRows = false;
            this.dgv_free.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.dgv_free.BackgroundColor = System.Drawing.Color.Gainsboro;
            this.dgv_free.CellBorderStyle = System.Windows.Forms.DataGridViewCellBorderStyle.Sunken;
            dataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            dataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv_free.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle13;
            this.dgv_free.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv_free.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dataGridViewTextBoxColumn6});
            this.dgv_free.Location = new System.Drawing.Point(350, 100);
            this.dgv_free.Name = "dgv_free";
            this.dgv_free.RowHeadersVisible = false;
            this.dgv_free.RowTemplate.DefaultCellStyle.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.dgv_free.RowTemplate.DefaultCellStyle.ForeColor = System.Drawing.Color.Black;
            this.dgv_free.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.dgv_free.Size = new System.Drawing.Size(103, 279);
            this.dgv_free.TabIndex = 3;
            this.dgv_free.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgv_CellEndEdit);
            // 
            // dataGridViewTextBoxColumn6
            // 
            dataGridViewCellStyle14.ForeColor = System.Drawing.Color.Black;
            this.dataGridViewTextBoxColumn6.DefaultCellStyle = dataGridViewCellStyle14;
            this.dataGridViewTextBoxColumn6.HeaderText = " ";
            this.dataGridViewTextBoxColumn6.MaxInputLength = 1;
            this.dataGridViewTextBoxColumn6.Name = "dataGridViewTextBoxColumn6";
            // 
            // FormMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(803, 465);
            this.Controls.Add(this.dgv_free);
            this.Controls.Add(this.groupBox_myMethod);
            this.Controls.Add(this.groupBox_criterial);
            this.Controls.Add(this.groupBox_params);
            this.Controls.Add(this.groupBox_libraryMethod);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(707, 500);
            this.Name = "FormMain";
            this.Text = "Случайные числа";
            this.groupBox_myMethod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_3_digit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_2_digit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_myMethod_1_digit)).EndInit();
            this.groupBox_libraryMethod.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_3_digit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_2_digit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_libMethod_1_digit)).EndInit();
            this.groupBox_params.ResumeLayout(false);
            this.groupBox_params.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_n_show)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown_n_generate)).EndInit();
            this.groupBox_criterial.ResumeLayout(false);
            this.groupBox_criterial.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv_free)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox_myMethod;
        private System.Windows.Forms.GroupBox groupBox_libraryMethod;
        private System.Windows.Forms.DataGridView dgv_myMethod_1_digit;
        private System.Windows.Forms.GroupBox groupBox_params;
        private System.Windows.Forms.NumericUpDown numericUpDown_n_generate;
        private System.Windows.Forms.Label label_n_generate;
        private System.Windows.Forms.Button button_generate_my;
        private System.Windows.Forms.NumericUpDown numericUpDown_n_show;
        private System.Windows.Forms.Label label_n_show;
        private System.Windows.Forms.Button button_show_my;
        private System.Windows.Forms.DataGridViewTextBoxColumn digits_1;
        private System.Windows.Forms.DataGridView dgv_myMethod_3_digit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn2;
        private System.Windows.Forms.DataGridView dgv_myMethod_2_digit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn1;
        private System.Windows.Forms.DataGridView dgv_libMethod_3_digit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn3;
        private System.Windows.Forms.DataGridView dgv_libMethod_2_digit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn4;
        private System.Windows.Forms.DataGridView dgv_libMethod_1_digit;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn5;
        private System.Windows.Forms.GroupBox groupBox_criterial;
        private System.Windows.Forms.TextBox textBox_criterial_lib_3;
        private System.Windows.Forms.TextBox textBox_criterial_lib_2;
        private System.Windows.Forms.TextBox textBox_criterial_lib_1;
        private System.Windows.Forms.TextBox textBox_criterial_my_3;
        private System.Windows.Forms.TextBox textBox_criterial_my_2;
        private System.Windows.Forms.TextBox textBox_criterial_my_1;
        private System.Windows.Forms.Button button_show_lib;
        private System.Windows.Forms.Button button_generate_lib;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.TextBox textBox_criterial_free;
        private System.Windows.Forms.DataGridView dgv_free;
        private System.Windows.Forms.Button button_calculate_critetials;
        private System.Windows.Forms.ComboBox comboBox_criterials;
        private System.Windows.Forms.DataGridViewTextBoxColumn dataGridViewTextBoxColumn6;
    }
}


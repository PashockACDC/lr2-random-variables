﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RandomVariable.Model;

namespace RandomVariable
{
    public partial class FormMain : Form
    {
        /// <summary>кол-во генерируемых чисел</summary>
        private int nGenerate = 10000;
        /// <summary>кол-во отображаемых чисел</summary>
        private int nShow = 10;

        /// <summary>выбранный критерий (по умолчанию - среднее)</summary>
        Criterials chosenCriterial = Criterials.AVERAGE;

        MyRandom r = new MyRandom();

        public FormMain()
        {
            InitializeComponent();

            dgv_free[0, 0].Style.Alignment = DataGridViewContentAlignment.MiddleCenter;
        }



        /************************
         ** ОБРАБОТЧИКИ КНОПОК **
         ************************/

        /// <summary>
        /// Смотрим, чтобы не нажимать кнопку,
        /// когда показано выбранное кол-во чисел
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void numericUpDown_n_show_ValueChanged(object sender, EventArgs e)
        {
            if (numericUpDown_n_show.Value == nShow)
            {
                button_show_my.Enabled = false;
            }
            else
            {
                button_show_my.Enabled = true;
            }
        }

        /// <summary>
        /// Проверка введённого числа в ячейку (для 1-о разрядного числа)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_1_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv_1 = (DataGridView)sender;
            int temp;
            if (!int.TryParse(dgv_1[e.ColumnIndex, e.RowIndex].Value.ToString(), out temp))
            {
                dgv_1[e.ColumnIndex, e.RowIndex].Value = 0;
            }
        }

        /// <summary>
        /// Проверка введённого числа в ячейку (для 2-х разрядного числа)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv_2 = (DataGridView)sender;
            try
            {
                if (int.Parse(dgv_2[e.ColumnIndex, e.RowIndex].Value.ToString()) < 10)
                {
                    dgv_2[e.ColumnIndex, e.RowIndex].Value = 10;
                }
            }
            catch (Exception ex)
            {
                dgv_2[e.ColumnIndex, e.RowIndex].Value = 10;
            }
        }

        /// <summary>
        /// Проверка введённого числа в ячейку (для 3-х разрядного числа)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_3_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv_3 = (DataGridView)sender;
            try
            {
                if (int.Parse(dgv_3[e.ColumnIndex, e.RowIndex].Value.ToString()) < 100)
                {
                    dgv_3[e.ColumnIndex, e.RowIndex].Value = 100;
                }
            }
            catch (Exception ex)
            {
                dgv_3[e.ColumnIndex, e.RowIndex].Value = 100;
            }
        }

        /// <summary>
        /// Проверка введённого числа в ячейку (для любого целого числа)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dgv = (DataGridView)sender;
            if (dgv[e.ColumnIndex, e.RowIndex].Value != null)
            {
                int temp;
                if (int.TryParse(dgv[e.ColumnIndex, e.RowIndex].Value.ToString(), out temp))
                {
                    dgv[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Black;
                }
                else
                {
                    dgv[e.ColumnIndex, e.RowIndex].Style.ForeColor = Color.Red;
                }
            }
        }

        /// <summary>
        /// Сгенерировать числа моим методом
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_generate_my_Click(object sender, EventArgs e)
        {
            int[] randNumbers1 = new int[nGenerate];
            int[] randNumbers2 = new int[nGenerate];
            int[] randNumbers3 = new int[nGenerate];
            for (int i = 0; i < nGenerate; i++)
            {
                randNumbers1[i] = r.NextInt_1_digit();
                randNumbers2[i] = r.NextInt_2_digit();
                randNumbers3[i] = r.NextInt_3_digit();
            }
            fillDGV(dgv_myMethod_1_digit, randNumbers1);
            fillDGV(dgv_myMethod_2_digit, randNumbers2);
            fillDGV(dgv_myMethod_3_digit, randNumbers3);
            textBox_criterial_my_1.Text = CriterialUtils.calculateCriteral(dgv_myMethod_1_digit, chosenCriterial)
                                                         .ToString();
            textBox_criterial_my_2.Text = CriterialUtils.calculateCriteral(dgv_myMethod_2_digit, chosenCriterial)
                                                         .ToString();
            textBox_criterial_my_3.Text = CriterialUtils.calculateCriteral(dgv_myMethod_3_digit, chosenCriterial)
                                                         .ToString();
        }

        /// <summary>
        /// Показать первые N чисел, сгенерированных моим методом
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_show_my_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Сгенерировать числа библиотечным методом
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_generate_lib_Click(object sender, EventArgs e)
        {
            int[] randNumbers1 = new int[nGenerate];
            int[] randNumbers2 = new int[nGenerate];
            int[] randNumbers3 = new int[nGenerate];
            Random r = new Random();
            for (int i = 0; i < nGenerate; i++)
            {
                randNumbers1[i] = (int)(r.NextDouble() * 10);
                randNumbers2[i] = 10 + (int)(r.NextDouble() * 90);
                randNumbers3[i] = 100 + (int)(r.NextDouble() * 900);
            }
            fillDGV(dgv_libMethod_1_digit, randNumbers1);
            fillDGV(dgv_libMethod_2_digit, randNumbers2);
            fillDGV(dgv_libMethod_3_digit, randNumbers3);
            textBox_criterial_lib_1.Text = CriterialUtils.calculateCriteral(dgv_libMethod_1_digit, chosenCriterial)
                                                         .ToString();
            textBox_criterial_lib_2.Text = CriterialUtils.calculateCriteral(dgv_libMethod_2_digit, chosenCriterial)
                                                         .ToString();
            textBox_criterial_lib_3.Text = CriterialUtils.calculateCriteral(dgv_libMethod_3_digit, chosenCriterial)
                                                         .ToString();
        }


        /// <summary>
        /// Показать первые N чисел, сгенерированных библиотечным методом
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_show_lib_Click(object sender, EventArgs e)
        {

        }





        public void fillDGV(DataGridView dgv, int[] numbers, bool showAll = true)
        {
            this.Invoke( (Action)( () =>
            {
                if (numbers.Length > 0)
                {
                    dgv.RowCount = numbers.Length;
                    int amount = showAll ? dgv.RowCount : (int)numericUpDown_n_show.Value;
                    for (int i = 0; i < amount; i++)
                    {
                        dgv[0, i].Value = numbers[i];
                    }
                }
            }));
        }

        /// <summary>
        /// Посчитать критерий для свободно вводимых чисел
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button_calculate_critetials_Click(object sender, EventArgs e)
        {
            textBox_criterial_free.Text = CriterialUtils.calculateCriteral(dgv_free, chosenCriterial)
                                                         .ToString();
        }

        /// <summary>
        /// Выбираем критерий для оценки случайности
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox_criterials_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch (((ComboBox)sender).Text)
	        {
                case "среднее арифметическое":
                    chosenCriterial = Criterials.AVERAGE;
                    break;
                case "мой частотный критерий":
                    chosenCriterial = Criterials.SOME_MY_CRITERIA;
                    break;
		        default:
                    chosenCriterial = Criterials.AVERAGE;
                    break;
	        }
            textBox_criterial_lib_1.Text = calculateCriteral(dgv_libMethod_1_digit, chosenCriterial).ToString();
            textBox_criterial_lib_2.Text = calculateCriteral(dgv_libMethod_2_digit, chosenCriterial).ToString();
            textBox_criterial_lib_3.Text = calculateCriteral(dgv_libMethod_3_digit, chosenCriterial).ToString();
            textBox_criterial_my_1.Text = calculateCriteral(dgv_myMethod_1_digit, chosenCriterial).ToString();
            textBox_criterial_my_2.Text = calculateCriteral(dgv_myMethod_2_digit, chosenCriterial).ToString();
            textBox_criterial_my_3.Text = calculateCriteral(dgv_myMethod_3_digit, chosenCriterial).ToString();
        }

        /// <summary>
        /// Посчитать критерий для указанной таблицы
        /// </summary>
        /// <param name="dgw_free"></param>
        /// <param name="chosen"></param>
        /// <returns></returns>
        private double calculateCriteral(DataGridView dgv, Criterials chosen)
        {
            try
            {
                return CriterialUtils.calculateCriteral(dgv, chosen);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


    }
}
